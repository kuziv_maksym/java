insert into team (id, name, city, comission, budget)
values (1, 'Lviv', 'Karpaty', 5, 200000),
 (2, 'Kyiv', 'Dynamo', 7, 2000000);

insert into player (id, expirience, firstName, lastName,
                    startdate, age, teamid, price)
values (1,10, 'Max','Kuziv', '2019-11-11', 20, 1, 50000),
 (2,10, 'Oleg','Patrol', '2018-11-11', 24, 1, 41000),
 (3,10, 'Kolya','Lastov', '2015-11-11', 28, 1, 35000),
 (4,10, 'Igor','Yaremko', '2019-11-11', 21, 2, 47000),
 (5,10, 'Andriy','Hingta', '2017-11-11', 23, 2, 39000),
 (6,10, 'Nazar','Kokorev', '2012-11-11', 24, 2, 410000);