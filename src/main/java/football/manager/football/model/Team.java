package football.manager.football.model;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Entity
@Table(name = "TEAM")
public class Team {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull
    private String city;
    @NotNull
    private String name;
    @Max(value = 10)
    @Min(value = 0)
    @NotNull
    private int comission;

    @NotNull
    private long budget;

    public Team() {
    }

    public Team(Integer id, String city, String name, int comission, long budget) {
        this.id = id;
        this.city = city;
        this.name = name;
        this.comission = comission;
        this.budget = budget;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getComission() {
        return comission;
    }

    public void setComission(int comission) {
        this.comission = comission;
    }

    public long getBudget() {
        return budget;
    }

    public void setBudget(long budget) {
        this.budget = budget;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Team team = (Team) o;
        return comission == team.comission && Double.compare(team.budget, budget) == 0 && Objects.equals(id, team.id) && Objects.equals(city, team.city) && Objects.equals(name, team.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, city, name, comission, budget);
    }
}
