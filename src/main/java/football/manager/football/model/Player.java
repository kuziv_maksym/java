package football.manager.football.model;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name="PLAYER")
public class Player {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull
    private int expirience;

    @Column(name = "firstname")
    @NotNull
    private String firstName;

    @Column(name = "lastname")
    @NotNull
    private String lastName;

    @Column(name ="startdate")
    private LocalDate startDate;

    @Column(name = "age")
    @Max(value = 100)
    @Min(value = 14)
    private int age;

    @Column(name="price")
    @Max(value = 100000000)
    @Min(value = 10000)
    private long price;

    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name="teamid")
    @NotNull
    private Team team;


    public Player(Integer id, int expirience, String firstName, String lastName, LocalDate startDate, int age, long price, Team team) {
        this.id = id;
        this.expirience = expirience;
        this.firstName = firstName;
        this.lastName = lastName;
        this.startDate = startDate;
        this.age = age;
        this.price = price;
        this.team = team;
    }

    public Player() {
    }

    public long getPrice() {
        this.price = (expirience * 100000) / age;
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getExpirience() {
        return expirience;
    }

    public void setExpirience(int expirience) {
        this.expirience = expirience;
    }


    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Player player = (Player) o;
        return expirience == player.expirience && age == player.age && Double.compare(player.price, price) == 0 && Objects.equals(id, player.id) && Objects.equals(firstName, player.firstName) && Objects.equals(lastName, player.lastName) && Objects.equals(startDate, player.startDate) && Objects.equals(team, player.team);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, expirience, firstName, lastName, startDate, age, price, team);
    }
}
