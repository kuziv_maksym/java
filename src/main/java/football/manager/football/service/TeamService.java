package football.manager.football.service;

import football.manager.football.model.Player;
import football.manager.football.model.Team;
import football.manager.football.repository.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TeamService {

    @Autowired
    private TeamRepository teamRepository;

    public List<Team> fetchTeams() {
        return teamRepository.findAll();
    }

    public Team fetchTeamById(Integer id) {
        return teamRepository.findTeamById(id);
    }

    public Team saveTeam(Team team) {
        return teamRepository.save(team);
    }

    public ResponseEntity deleteTeamById(Integer id) {
        teamRepository.deleteById(id);
        return ResponseEntity.ok().build();
    }

}
