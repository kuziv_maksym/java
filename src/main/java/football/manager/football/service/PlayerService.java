package football.manager.football.service;

import football.manager.football.model.Player;
import football.manager.football.repository.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PlayerService {

    @Autowired
    private PlayerRepository playerRepository;

    public List<Player> fetchPlayerByTeamId(Integer id){
        return playerRepository.findAllByTeamId(id);
    }

    public Player fetchPlayerById(Integer id) {
        return playerRepository.findPlayerById(id);
    }

    public Player savePlayer(Player player) {
        return playerRepository.save(player);
    }

    public ResponseEntity deletePlayerById(Integer id) {
        playerRepository.deleteById(id);
        return ResponseEntity.ok().build();
    }


}
