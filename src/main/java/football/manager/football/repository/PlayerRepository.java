package football.manager.football.repository;

import football.manager.football.model.Player;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PlayerRepository extends JpaRepository<Player, Integer>{

    @Query(value = "select * from Player where teamID=?1", nativeQuery = true)
    List<Player> findAllByTeamId(Integer id);

    Player findPlayerById(Integer id);

}
