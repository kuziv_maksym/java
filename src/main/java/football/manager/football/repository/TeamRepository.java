package football.manager.football.repository;

import football.manager.football.model.Team;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TeamRepository extends JpaRepository<Team, Integer> {
    Team findTeamById(Integer id);
    void deleteById(Integer id);
}
