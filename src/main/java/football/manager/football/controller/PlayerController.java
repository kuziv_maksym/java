package football.manager.football.controller;

import football.manager.football.model.Player;
import football.manager.football.model.Team;
import football.manager.football.service.PlayerService;
import football.manager.football.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/team")
@CrossOrigin(value = "*")
public class PlayerController {

    @Autowired
    private PlayerService playerService;

    @Autowired
    private TeamService teamService;

    @GetMapping("/players/{id}")
    public List<Player> fetchPlayers(@PathVariable Integer id){
        List<Player> players = playerService.fetchPlayerByTeamId(id);
        return players;
    }

    @GetMapping("/player/{id}")
    public Player fetchPlayerById(@PathVariable Integer id) {
        Player p = playerService.fetchPlayerById(id);
        return p;
    }

    @PutMapping("/player/{id}")
    public ResponseEntity<Player> updatePlayer(@PathVariable Integer id,
                                               @Valid @RequestBody Player playerDetails) {
        Player player = playerService.fetchPlayerById(id);
        player.setFirstName(playerDetails.getFirstName());
        player.setLastName(playerDetails.getLastName());
        player.setAge(playerDetails.getAge());
        player.setExpirience(playerDetails.getExpirience());
        player.setStartDate(playerDetails.getStartDate());
        Team from = player.getTeam();
        Team to = playerDetails.getTeam();
        from.setBudget(from.getBudget() + player.getPrice() + ((player.getPrice() * to.getComission())/100));
        to.setBudget(to.getBudget() - player.getPrice() - ((player.getPrice()* to.getComission())/100));
        teamService.saveTeam(from);
        teamService.saveTeam(to);
        player.setTeam(playerDetails.getTeam());
        playerService.savePlayer(player);
        return ResponseEntity.ok(player);
    }

    @DeleteMapping("/player/{id}")
    public ResponseEntity deleteById(@PathVariable Integer id) {
        playerService.deletePlayerById(id);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/player")
    public Player addPlayer(@Valid @RequestBody Player player) {
        return playerService.savePlayer(player);
    }
}
