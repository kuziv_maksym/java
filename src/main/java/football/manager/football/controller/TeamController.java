package football.manager.football.controller;

import football.manager.football.model.Team;
import football.manager.football.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:4200")
public class TeamController {

    @Autowired
    private TeamService teamService;

    @GetMapping("/team")
    public List<Team> fetchTeams() {
        List<Team> teams = teamService.fetchTeams();
        return teams;
    }

    @GetMapping("/team/{id}")
    public Team fetchTeamById(@PathVariable Integer id) {
        Team team = teamService.fetchTeamById(id);
        return team;
    }

    @PostMapping("/team")
    public Team addTem(@RequestBody Team team) {
        return teamService.saveTeam(team);
    }

    @PutMapping("/team/{id}")
    public ResponseEntity<Team> updateTeam(@PathVariable Integer id,
                                           @Valid @RequestBody Team teamDetails) {
        Team team = teamService.fetchTeamById(id);
        team.setBudget(teamDetails.getBudget());
        team.setComission(teamDetails.getComission());
        team.setId(teamDetails.getId());
        teamService.saveTeam(team);
        return ResponseEntity.ok(team);
    }

    @DeleteMapping("/team/{id}")
    public ResponseEntity deleteTeamById(@PathVariable Integer id) {
        teamService.deleteTeamById(id);
        return ResponseEntity.ok().build();
    }
}
